package com.core.java.collections.abstracttutoral;


/*
 *   @Created 05/11/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import javax.crypto.Mac;
import javax.xml.xpath.XPath;

public class GateWayAbs {

    public static void main(String[] args) {
        DellXPS xps  = new DellXPS();
        xps.hasMonitor();
        xps.hasHDMI();

        IBMThinkPad ibmThinkPad = new IBMThinkPad();
        ibmThinkPad.hasMonitor();
        ibmThinkPad.hasMouseStick();

        MacBookPro macBookPro = new MacBookPro();
        macBookPro.hasMonitor();
        macBookPro.hasTouchBar();


    }


}
