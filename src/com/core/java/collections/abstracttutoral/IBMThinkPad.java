package com.core.java.collections.abstracttutoral;


/*
 *   @Created 05/11/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class IBMThinkPad extends Computer{
    /**
     * Abstract method
     */
    @Override
    public void hasMonitor() {
        System.out.println("Think pad has classic LCD monitor");
    }

    @Override
    public void hasKeyword() {
        System.out.println("IBM ThinkPad has normal attached keyword");
    }

    @Override
    public void hasHardDrive() {
        System.out.println("Think pad HAS classical Tosiba Hard Drive");
    }

    @Override
    public void hasCPU() {
        System.out.println("It has inter Process");
    }


    /**
     * Regular Method
     */
    @Override
    public void hasMouseStick() {
        System.out.println("This computer has  joy Stick based mouse");
    }
}
