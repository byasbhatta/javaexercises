package com.core.java.collections.abstracttutoral;


/*
 *   @Created 05/11/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public abstract class Computer {

    /**
     * Abstract method
     */
    public abstract void hasMonitor();
    public abstract void hasKeyword();
    public abstract void hasHardDrive();
    public abstract void hasCPU();

    /**
     * Regular Method
     */
    public void hasHDMI(){
        System.out.println("This Computer has HDMI port");

    }

    public void hasTouchPad(){
        System.out.println(" This Computer has Modern Touchpad");
    }

    public void hasMouseStick(){
        System.out.println("This computer has  joy Stick based mouse");
    }

    public void hasTouchBar(){
        System.out.println(" This computer has touch bar for volume, mucis and other quick lunch application");
    }
}
