package com.core.java.collections.abstracttutoral;


/*
 *   @Created 05/11/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class DellXPS extends Computer{
    /**
     * Abstract method
     */
    @Override
    public void hasMonitor() {
        System.out.println("Dell XPS has Touch screen monitor");
    }

    @Override
    public void hasKeyword() {
        System.out.println("Dell XPS has backlit based keyboard");
    }

    @Override
    public void hasHardDrive() {
        System.out.println("Dell XPS  has SSD drive");
    }

    @Override
    public void hasCPU() {
        System.out.println("It has Intel CPU");
    }


}
