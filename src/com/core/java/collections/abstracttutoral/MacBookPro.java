package com.core.java.collections.abstracttutoral;


/*
 *   @Created 05/11/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class MacBookPro extends Computer {
    /**
     * Abstract method
     */
    @Override
    public void hasMonitor() {
        System.out.println(" It has OLED screen");
    }

    @Override
    public void hasKeyword() {
        System.out.println("It has normal QWERTY Keyboard");

    }

    @Override
    public void hasHardDrive() {
        System.out.println("It has samsung hard drive");

    }

    @Override
    public void hasCPU() {
        System.out.println("It has custom archtecure based intel processor");

    }

    @Override
    public void hasTouchPad() {
        super.hasTouchPad();
    }

    @Override
    public void hasTouchBar() {
        super.hasTouchBar();
    }
}
