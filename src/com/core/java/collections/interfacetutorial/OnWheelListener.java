package com.core.java.collections.interfacetutorial;


/*
 *   @Created 05/07/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */

/**
 * This is abstraction for vehicle movement
 */

public interface OnWheelListener {
    /**
     * Call backs for listening when the wheel starts to roll
     */
    public void onStart();

    /**
     * Call backs for listerning when the wheel stop rolling
     */
    public void onStop();
    /**
     * Call backs for listen  when the wheel acceleration of the wheel
     */

    public void onSpeed();

    /**
     * Call backs for listen when the break applied on the wheel
     */
    public void onBreak();
}
