package com.core.java.collections.interfacetutorial;


/*
 *   @Created 05/07/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class Phone implements OnNetworkListener{
    @Override
    public void getNetworkInfo(String NetworkProvider) {
        System.out.println();
    }

    @Override
    public void getSignalStrength(int veryStrong, int strong, int medium, int weak) {


    }

    @Override
    public void getPhoneNumber(String phoneNumber) {
        System.out.println();
    }

    @Override
    public void getIME(String imeTracker) {
        System.out.println();
    }
}
