package com.core.java.collections.interfacetutorial;


/*
 *   @Created 05/07/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class Vehicle  implements OnWheelListener{

    /**
     * Call backs for listening when the wheel starts to roll
     */
    @Override
    public void onStart() {
        System.out.println("This is  starting a roll");
    }

    /**
     * Call backs for listening when the wheel stop rolling
     */
    @Override
    public void onStop() {
        System.out.println("The Wheel is stopping when the break applied");

    }

    /**
     * Call backs for listen  when the wheel acceleration of the wheel
     */
    @Override
    public void onSpeed() {
        System.out.println("The wheel speed up when  press the accelerate");

    }

    /**
     * Call backs for listen when the break applied on the wheel
     */
    @Override
    public void onBreak() {
        System.out.println(" When the clause is  applied the wheel slowly stop");

    }
}
