package com.core.java.collections.interfacetutorial;


/*
 *   @Created 05/07/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public interface OnNetworkListener {

    public void getNetworkInfo(String NetworkProvider);

    public void getSignalStrength(int veryStrong, int strong, int medium, int weak);


    public void getPhoneNumber(  String phoneNumber);

    public void getIME( String imeTracker);
}

