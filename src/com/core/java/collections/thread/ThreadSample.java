package com.core.java.collections.thread;


/*
 *   @Created 05/12/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class ThreadSample extends Thread {

    private boolean killThread;
    @Override
    public synchronized void start() {
        super.start();
        System.out.println("Thread is starting");
    }


    @Override
    public void run() {
        super.run();
        while (!killThread) {
            System.out.println("Thread is running");

            try {
                System.out.println("Thread is sleeping");
                sleep(10000);
                System.out.println("Thread is wake");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        System.out.println("The programme is stopped");
        }
        public void kill(){
        killThread = true;
        }
    }

