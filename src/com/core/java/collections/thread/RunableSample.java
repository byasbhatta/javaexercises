package com.core.java.collections.thread;


/*
 *   @Created 05/12/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class RunableSample implements Runnable{

    @Override
    public void run() {
        System.out.println("The runnable is Running");
    }
}
