package com.core.java.collections.thread;


/*
 *   @Created 05/12/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class GateWay {
    public static void main(String[] args) {
        // ThreadSample threadSample = new ThreadSample();
        // threadSample.setDaemon(false);
        // threadSample.start();
        //threadSample.run();
        // threadSample.kill();

        RunableSample runableSample = new RunableSample();
        Thread thread = new Thread(runableSample, "Thread one");
        thread.start();

        Thread thread2 = new Thread(runableSample, "Thread 2");
        thread2.start();
        System.out.println(thread.getName());
        System.out.println(thread2.getName());




    }
}
