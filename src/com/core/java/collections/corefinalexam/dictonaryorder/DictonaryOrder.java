package com.core.java.collections.corefinalexam.dictonaryorder;


/*
 *   @Created 05/16/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class DictonaryOrder {
    /**
     * Sorting the word
     */
    public void Sort() {
        String[] words = {" giraffe", " zebra", " cow", " ape", " buffalo", " tiger", " lion", " donkey"};

        for (int i = 0; i < 7; ++i) {
            for (int j = i + 1; j < 7; ++j) {
                    // using compareTo Method
                if (words[i].compareTo(words[j]) > 0) {

                    // Swaping the word
                    String twmpwords = words[i];
                    words[i] = words[j];
                    words[j] = twmpwords;
                }
            }
        }
        System.out.println("Dictonary Order are given below: ");
        for (int i =0; i < 7; i ++){
            System.out.println(words[i]);
        }
    }

}
