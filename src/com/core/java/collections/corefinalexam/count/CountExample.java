package com.core.java.collections.corefinalexam.count;


/*
 *   @Created 05/16/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class CountExample {

        public void count(String x){
            char[] chars = x.toCharArray();
            int letters = 0, numbers= 0, whiteSpaces=0, specialCharacter = 0;
            for(int i = 0; i < x.length(); i ++){
                if (Character.isLetter(chars[i])){
                    letters ++;

                }
                else if (Character.isDigit(chars[i])){
                    numbers ++;

                }
                else if ( Character.isWhitespace(chars[i])){
                    whiteSpaces++;

                }
                else {
                    specialCharacter++;

                }
            }
            System.out.println("Total Letters:  "+ letters);
            System.out.println("Total Number : " + numbers);
            System.out.println("Total whitespaces : "+ whiteSpaces);
            System.out.println("Total Special characters : " + specialCharacter);

        }


}
