package com.core.java.collections.corefinalexam.calendar;


/*
 *   @Created 05/16/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.time.LocalTime;


public class LocalDate {

    public void weekDayWithLocalDate() {
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
    }
}
