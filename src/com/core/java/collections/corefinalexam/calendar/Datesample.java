package com.core.java.collections.corefinalexam.calendar;


/*
 *   @Created 05/17/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.text.SimpleDateFormat;
import java.util.Date;

public class Datesample {
    public void convertToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MM dd, yyyy");
        String dateString = format.format(date);
        System.out.println(dateString);
    }
}
