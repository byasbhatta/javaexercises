package com.core.java.collections.corefinalexam.genericinterface;


/*
 *   @Created 05/16/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */

@FunctionalInterface
public interface GenericInterace<T> {
    void get(T t);
}
