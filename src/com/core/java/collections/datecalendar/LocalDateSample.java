package com.core.java.collections.datecalendar;


/*
 *   @Created 05/14/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import jdk.swing.interop.SwingInterOpUtils;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateSample {

    public void displayLocalDate(){
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
    }

    public void displayLocalTime(){
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
    }

    public void displayLocalDayTime(){
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
    }


    public void convertToLocalDate(String dateString){
        System.out.println(dateString);
     LocalDate localDate = LocalDate.parse(dateString);
        try {
            System.out.println(localDate);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }


    }
}
