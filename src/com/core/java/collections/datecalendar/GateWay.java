package com.core.java.collections.datecalendar;


/*
 *   @Created 05/13/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.Date;

public class GateWay {

    public static void main(String[] args) {
//        DateSample dateSample = new DateSample();
//        dateSample.displayDate();
//        dateSample.convertToString(new Date());
/*
        dateSample.convertToDate("Jan 1, 2020");
        dateSample.displayCalendar();*/

        LocalDateSample localDateSample = new LocalDateSample();
        localDateSample.displayLocalDate();
        localDateSample.displayLocalTime();
        localDateSample.displayLocalDayTime();

        localDateSample.convertToLocalDate("1990-01-10");
    }
}
