package com.core.java.collections.datecalendar;


/*
 *   @Created 05/13/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateSample {

    public void displayDate() {
        Date date = new Date();
        System.out.println(date);
    }

   public void convertToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = format.format(date);
        System.out.println(dateString);
    }

    public void convertToDate(String dateString) {

        SimpleDateFormat formatter = new SimpleDateFormat("MM dd, yyyy");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

    }

    public void displayCalendar(){
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar);

    }

   /* public void convertToCalendar(String DateString){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = Calendar.
    }
*/


}

