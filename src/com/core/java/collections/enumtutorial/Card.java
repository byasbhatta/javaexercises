package com.core.java.collections.enumtutorial;


/*
 *   @Created 05/14/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.time.LocalDateTime;

public class Card {

    private String name;
    private String description;
    private LocalDateTime created;
    private Status status;

    public Card(String name, String description, LocalDateTime created, Status status) {
        this.name = name;
        this.description = description;
        this.created = created;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
