package com.core.java.collections.enumtutorial;


/*
 *   @Created 05/14/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.time.LocalDateTime;

public class GateWay {
    public static void main(String[] args) {
        Card card = new Card("New Card","Card Description", LocalDateTime.now(), Status.URGENT);

        SampleEnum sampleEnum = new SampleEnum();
     /*  sampleEnum.checkCard(card);

        sampleEnum.checkCardStatus(card);
        sampleEnum.checkStatusOptions();

        sampleEnum.checkEnumSet();*/

        sampleEnum.checkEnumMap();


    }
}
