package com.core.java.collections.enumtutorial;


/*
 *   @Created 05/14/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public enum Status {
    URGENT("Very Important"),
    HIGH("Important"),
    MEDIUM("Less Important"),
    LOW("Least important");

    public String tag;

    Status(String tag){
        this.tag =tag;
    }

    public String getTag(){
        return tag;
    }
}
