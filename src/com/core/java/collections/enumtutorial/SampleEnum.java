package com.core.java.collections.enumtutorial;


/*
 *   @Created 05/14/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.EnumMap;
import java.util.EnumSet;

import static com.core.java.collections.enumtutorial.Status.*;

public class SampleEnum {

    public void checkCard(Card card) {
        if (card.getStatus() == URGENT) {
            System.out.println("This is Urgent card, must be fixed by today");
        }

    }

    public void checkCardStatus(Card card) {
        switch (card.getStatus()) {
            case URGENT:
                System.out.println("This is Urgent card");
                break;

            case HIGH:
                System.out.println("This is important card");
                break;

            case MEDIUM:
                System.out.println("This is medium card");
                break;

            case LOW:
                System.out.println(" This is Low priority card");
                break;
        }
    }


    public void checkStatusOptions() {
        for (Status status : Status.values()) {
            System.out.println(status);
            System.out.println(status.getTag());
        }
    }

    public void checkEnumSet() {
        EnumSet<Status> enumSet = EnumSet.of(HIGH, URGENT, MEDIUM, LOW);
        for (Status status : enumSet) {
            System.out.println(status);
        }
    }

    public void checkEnumMap(){
        EnumMap<Status,Integer>enumMap= new EnumMap<Status, Integer>(Status.class);
        enumMap.put(URGENT, 1);
        enumMap.put(LOW, 4);

        for(Integer i : enumMap.values()){
            if(i==4){
                System.out.println("This is Low Status");
            }
        }

    }

}
