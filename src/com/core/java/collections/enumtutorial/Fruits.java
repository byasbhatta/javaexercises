package com.core.java.collections.enumtutorial;


/*
 *   @Created 05/15/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public enum Fruits {
    APPLE,
    ORANGE,
    MELON,
    GUAVA
}
