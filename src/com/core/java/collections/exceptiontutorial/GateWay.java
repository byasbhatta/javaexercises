package com.core.java.collections.exceptiontutorial;


/*
 *   @Created 05/12/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */




public class GateWay {
    public static void main(String[] args) {
        ExceptionHandaling exceptionHandaling = new ExceptionHandaling();
        /*exceptionHandaling.checkNullPointerException();

        exceptionHandaling.checkArithmeticException(10, 0);*/

        exceptionHandaling.checkNumberFormatException("aeiou");

        exceptionHandaling.checkArrayIndexOutOfBoundException();


        exceptionHandaling.checkIndexOutOfBoundException();
    }


}
