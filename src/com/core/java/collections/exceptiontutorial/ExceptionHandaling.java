package com.core.java.collections.exceptiontutorial;


/*
 *   @Created 05/12/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.ArrayList;
import java.util.List;

public class ExceptionHandaling {

    public void checkNullPointerException() {

        try {
            String x = null;
            System.out.println(x.length());
        } catch (NullPointerException e) {
            System.out.println("Please avoid null String");
        }


    }

    public void checkArithmeticException(int x, int y) {
        try {
            int z = x / y;
        } catch (ArithmeticException e) {
            System.out.println("A number divided by zero is infinity which is not supported");
        }
    }


    public void checkNumberFormatException(String s) {
        try {
            int number = Integer.parseInt(s);
            System.out.println(number);

        } catch (NumberFormatException e) {
            System.out.println("invalid input");
        }


    }

    public void checkArrayIndexOutOfBoundException() {

        try {
            char[] vowel = {'a', 'e', 'i', 'o', 'u'};
            System.out.println(vowel[8]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index number must be less than array size");
        }


    }

    public void checkIndexOutOfBoundException() {

        try {
            List<String> stringList = new ArrayList<>();
            String a = new String("abcdef");
            String b = new String("cdefg");
            stringList.add(a);
            stringList.add(b);

            System.out.println(stringList.size());
            System.out.println(stringList.get(stringList.size() + 1));

        } catch (IndexOutOfBoundsException e) {
            System.out.println("Index must be less than list size");
        }
    }
}
