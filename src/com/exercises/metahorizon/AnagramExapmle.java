  /*Check if the String is anagram or not
        •	Eg:
        •	tap and pat
        •	neo and one
        •	on and no
        •	break and baker
        •	allergy and gallery

*/

  package com.exercises.metahorizon;

/*
 *   @Created 04/23/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


  import java.util.Arrays;

  public class AnagramExapmle {
    static void isAnagram(String one , String two){
    String  string1 = one.replaceAll("\\s ", " ");
    String  string2 = two.replaceAll("\\s  ", " ");

    boolean status = true;
    if(string1.length()!= string2.length()) {
      status = false;
    }
    else{
      char[] ArrayString1 = string1.toLowerCase().toCharArray();
      char[] ArrayString2 = string2.toLowerCase().toCharArray();
      Arrays.sort(ArrayString1);
      Arrays.sort(ArrayString2);
      status = Arrays.equals(ArrayString1, ArrayString2);
    }
    if (status){
      System.out.println(string1 + " and "  + string2 +  " are anagrams");
    }else{
      System.out.println(string1 + " and "  + string2 + " are not anagrams");
    }

  }

    public static void main(String[] args) {
      isAnagram("tap" ,  "pat");
      isAnagram("neo" ,  "one");
      isAnagram("chitta" ,  "khetia");
    }
}
