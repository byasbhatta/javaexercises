// Questions: The variable of int array is as follow int [ ] intArray={1,7,9,4,2,8,3}.
// Rearrange the array in the ascending order.

package com.exercises.metahorizon;


/*
 *   @Created 04/23/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.Arrays;

public class ArrrayAsseOrder {
    public static void main(String[] args) {

        //  total Array elements
        int [ ] intArray={1,7,9,4,2,8,3};
        Arrays.sort(intArray);

        System.out.println(Arrays.toString(intArray));
    }
}
