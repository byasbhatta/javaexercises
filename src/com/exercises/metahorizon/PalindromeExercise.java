
//Check if  the String is palindrome or not.

package com.exercises.metahorizon;


/*
 *   @Created 04/23/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.Scanner;

public class PalindromeExercise {

    public static void main(String[] args) {
        String str;
        String rev= "";
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter A String");
        str =sc.nextLine();

        int length = str.length();

        for (int i = length -1; i >=0; i--)
            rev = rev +str.charAt(i);

        if (str.equals(rev))
            System.out.println(str + " is palindrom");
        else
        System.out.println( str +" is not palindrom");

    }

}
