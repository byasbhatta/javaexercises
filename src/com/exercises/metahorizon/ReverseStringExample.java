 /*Write a program to display the text in reverse
     •	Input : This is a cat
     •	Output : tac a si sihT
*/

 package com.exercises.metahorizon;

/*
 *   @Created 04/23/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class ReverseStringExample {
    public static void main(String[] args) {
        StringBuffer rs = new StringBuffer("This is a cat");
        System.out.println(rs.reverse());

    }
}
