package com.exercises.metahorizon;


/*
 *   @Created 05/17/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class GateWayBoolean {
    public static void main(String[] args) {
        LogicGate logicGate = new LogicGate();
        logicGate.checkGate();
        logicGate.checkRoom();
    }
}
