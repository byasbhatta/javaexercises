package com.ecommerce.april.collection;


/*
 *   @Created 05/01/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.ArrayList;
import java.util.List;

public class WhileLoop {

    List<Employees> employeesList = new ArrayList<Employees>();
    List<Employees>employeesList2 = new ArrayList<>();

    public void populate(){
        Employees employee = new Employees(" Mike Singh", "Software Developer", 120000);
        Employees employee2 = new Employees(" Mike ", "Data Analyst", 120000);
        Employees employee3 = new Employees(" simu  louh", "Security Analyst", 150000);


        // Way to add on list
        employeesList.add(employee);
        employeesList.add(employee2);
        employeesList.add(employee3);

        //to clear data in list
        // employeeList.clear();

        //  employeesList.size();

        // to get an object at a specific position in list
     //   Employees emp = employeesList.get(0);

        //Add List from other list of similar data type
    //    employeesList2.addAll(employeesList);


    }
    public void displayEmployee(){
        int i =0;
        while ( i <employeesList.size()){
            System.out.println(employeesList.get(i).getFullName() + " - " +employeesList.get(i).getDesignation());
            i++;

        }
    }

}
