package com.ecommerce.april.collection;

import java.util.List;

public class Sample {
    // primitive data type

    // instance variable
     private int i;
     private double d;
     private char [] c;
     private boolean flag;
     private long l;
     private float f;

     // non primitive data type

    private String text;
    private List<String> stringList;


    public int firstMethod(int i) {
        System.out.println(i);
        return i;

    }

    public void secondMethod(int i, int j) {
        // local variable
        String s = "local variable";
        System.out.println(i + j);
    }
}
