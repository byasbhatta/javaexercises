package com.ecommerce.april.collection;


/*
 *   @Created 05/04/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */

/**
 * This is parent class for inheritance
 */
public class Father {
    /**
     * has black silky hair
     */

    public void hasBlackHair(){
        System.out.println("He has black Silky hair");
    }

    /**
     * has good height
     */
    public void isTaller(){
        System.out.println("He is 6.3 feet");
    }

    /**
     * Own multiple real estate
     */
    public void ownThreeEstates(){
        System.out.println("He own 3 real estate on different location");
    }

    /**
     * has a good bank balance
     */
    public static void ownCertainBankBalance() {
        System.out.println("He has 6 figure bank balance");

    }





}
