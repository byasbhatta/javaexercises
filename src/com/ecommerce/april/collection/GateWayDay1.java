package com.ecommerce.april.collection;


/*
 *   @Created 05/01/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class GateWayDay1 {
    public static void main(String[] args) {

        /*WhileLoop whileloop = new WhileLoop();
        whileloop.populate();
        whileloop.displayEmployee();
        */

        DoWhileLoop dowhileloop = new DoWhileLoop();
        dowhileloop.populate();
        dowhileloop.display();
        }

    }

