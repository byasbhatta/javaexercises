package com.ecommerce.april.collection;


/*
 *   @Created 05/03/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.HashMap;

public class HashMapSample {
    private HashMap<Integer, String> hashMap = new HashMap<>();

    public void populate(){
        hashMap.put(1, "One");
        hashMap.put(2, "Two");
        hashMap.put(70, "seventy");
        hashMap.put(4, "Four");
        hashMap.put(20, "Twenty");

    }
    public void display(){
        for (String value : hashMap.values()){
            System.out.println(value);
        }
    }

}
