package com.ecommerce.april.collection;


/*
 *   @Created 05/03/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class GateWayDay2 {
    public static void main(String[] args) {
        HashMapSample hashmapsample = new HashMapSample();
        hashmapsample.populate();
        hashmapsample.display();
    }
}
