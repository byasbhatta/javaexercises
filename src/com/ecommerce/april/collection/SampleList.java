package com.ecommerce.april.collection;


/*
 *   @Created 05/06/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.ArrayList;

public class SampleList {
    private ArrayList<String>arrayList = new ArrayList<>();

    public void populate(){
        arrayList.add("Hello");
        arrayList.add("World");
        arrayList.add("This is Java");
    }

    public void arrayListUseCase(){

    }

    public static class GateWayWeeek1 {
        public static void main(String[] args) {
            //Week 2
            /*Numbers num = new Numbers( 6, 3);
            num.sum();
            num.compare();*/

           /* Weeks week = new Weeks();
            week.display(6);*/

            Months months = new Months();
            months.displayMonths();
        }

    }

    public static class Months {
        private String []monthsArray= {"Jan", "Feb", "Mar", "Apr", "May",
                                        "Jun", "Jul", "Aug","Sep", "Oct", "Nov","Dec"};
        public void displayMonths(){
            for(int i=0;i<monthsArray.length; i++){
                System.out.println(monthsArray[i]);

            }
        }

    }
}
