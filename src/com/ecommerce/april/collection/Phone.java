package com.ecommerce.april.collection;


/*
 *   @Created 05/04/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class Phone {

    public void hasNetWorkCarrier(){
        System.out.println("This iphone has T-mobile network ");
    }

    public void hasSignal(){
        System.out.println("This phone has a very good signal");
    }

    public void hasPhoneNumber(){
        System.out.println("This phone has 10 digit number");
    }

    public void hasPhysicalDialPad(){
        System.out.println("This is old phone with physical dial pad");
    }


}
