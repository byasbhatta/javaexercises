package com.ecommerce.april.collection;


/*
 *   @Created 05/01/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


public class Employees {

    private String fullName;
    private String designation;
    private int salary;


    public Employees(String fullName, String designation, int salary) {
        this.fullName = fullName;
        this.designation = designation;
        this.salary = salary;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
