package com.ecommerce.april.collection;


/*
 *   @Created 05/03/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */


import java.util.ArrayList;
import java.util.List;

public class DoWhileLoop {

    List<Employees> employeesList = new ArrayList<Employees>();

    public void populate() {
        Employees employee = new Employees(" Mike Singh", "Software Developer", 120000);
        Employees employee2 = new Employees(" Mike ", "Data Analyst", 120000);
        Employees employee3 = new Employees(" simu  louh", "Security Analyst", 150000);

        employeesList.add(employee);
        employeesList.add(employee2);
        employeesList.add(employee3);
    }


    public void display() {
        // set counter
        int i = 0;
        do {

            // statements
            System.out.println(employeesList.get(i).getFullName() + " - " + employeesList.get(i).getDesignation());
            i++;

        } while (i < employeesList.size());
    }
}

