package com.ecommerce.april.collection;


/*
 *   @Created 05/04/2020
 *   @Project JavaExercises
 *   @Author  Byas Work PC
 */

/**
 * This is a child class which inherit from father class
 */

public class Son extends Father{
    @Override
    public void hasBlackHair() {
        super.hasBlackHair();
    }
}
